//
//  Extensions.swift
//  Playlog
//
//  Created by Admin on 2019-07-15.
//  Copyright © 2019 Navpreet. All rights reserved.
//

import Foundation
import UIKit


// MARK: Range
extension ClosedRange where Bound: FixedWidthInteger {
    
    /**
     Method to generate the array of integers within the given range
     
     - Parameter n: Number of elements to generate
     - Returns: Array of random numbers
     */
    func randomElements(_ n: Int) -> [Bound] {
        guard n > 0 else { return [] }
        return (0..<n).map { _ in .random(in: self) }
    }
    var randomElement: Bound { return .random(in: self) }
}

// MARK: CGPoint
extension CGPoint {
    
    /**
     Method to find the distance between 2 CGPoints
     
     - Parameter point: Second CGPoint
     - Returns: CGFloat - Distance between 2 points
     */
    func distanceFromCGPoint(point: CGPoint) -> CGFloat {
        
        return sqrt(pow(self.x - point.x,2) + pow(self.y - point.y,2))
    }
}

// MARK: UIVIew
extension UIView {
    
    /**
     Make view circular
     
     - Precondition: Height and Width of the view should be equal to make it circle
     */
    func circularView() {
        
        self.layer.cornerRadius =  self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    /**
    Set background and tint color of the button
     
     */
    func buttonDesign() {
        
        self.backgroundColor = .lightText
        self.tintColor = .black
    }
}
