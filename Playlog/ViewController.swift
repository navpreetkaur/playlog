//
//  ViewController.swift
//  Playlog
//
//  Created by Admin on 2019-07-13.
//  Copyright © 2019 Navpreet. All rights reserved.
//

import UIKit
import Photos

class ViewController: UIViewController {
    
    // Wheel in the middle of the screen
    @IBOutlet weak var wheel: UIView!
    
    // Button to drag around the wheel (Display degrees relative to wheel center)
    @IBOutlet weak var dragMeButton: UIButton!
    
    // Background Image view
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    // Collection View to show the list of random Images
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    // Center of the `dragMeButton`
    var buttonCenter: CGPoint?
    
    // Degree of button relative to the wheel
    var buttonDegree: Int?
    
    // Array of UIImage to show in the list(UICollectionView)
    var imageArray = [UIImage]()
    
    // Array of Indexes from the PHAsset. These indexes corresponds to the `imageArray`
    var imageArrayIndex: [Int]?
    
    // Image counter to keep track of which image is being displayed in the `backgroundImageView`
    var imageCounter: Int = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setUpUI()
        setUpButtonDrag()
        
        // Register the custom Xib to show in the CollectionView
        self.imageCollectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
    }
    
    func setUpUI() {
        
        // Make UIView circular - Square in Main.storyboard
        self.wheel.circularView()
        
        // Set the background and title color of the button
        self.dragMeButton.buttonDesign()
        
        // Set the `dragMeButton` to the center of the screen on top of the wheel
        self.dragMeButton.center = self.view.center
    }
    
    func setUpButtonDrag() {
        
        self.buttonCenter = self.wheel.center
        
        // Set the PanGesture on `dragMeButton` to drag it around wheel
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panButton))
        self.dragMeButton.addGestureRecognizer(pan)
    }
    
    // Action to be performed on `dragMeButton` tap
    @IBAction func dragButtonPressed(_ sender: Any) {
        
        // P1
        self.rotateButtonRandomAmount()
        
        // P2
        // Generate number berween 10 to 20 (X - Number of Photos to be loaded randomly from camera roll)
        let X = (10 ... 20).randomElement()
        
        // Get X number of photos from the Photo Library and store the images in the imageArray
        self.imageArray = self.getImagesFromPhotos(numberOfImages: X ?? 0) ?? [UIImage]()
        
        // Reload the collection view with new list of image array
        self.imageCollectionView.reloadData()
        
        // Set the first image in the array as background image
        self.imageCounter = 0
        if self.imageArray.count > 0 {
            self.backgroundImageView.image = self.imageArray[0]
        }
    }
    
    // Action to be performed while dragging the button around.
    @objc func panButton(pan: UIPanGestureRecognizer) {
        
        // On beginning, save the previous center point to `buttonCenter` so that on failed or cancelled pan, we can take the button back to the original position
        if pan.state == .began {
            self.buttonCenter = self.dragMeButton.center
        } else if pan.state == .failed || pan.state == .cancelled // Move the button back to the original position
        {
            self.dragMeButton.center = buttonCenter ?? CGPoint(x: 0.0,y: 0.0)
        } else if pan.state == .ended // When drag ended -> Set the background Image
        {
            self.setBackgroundImage()
        } else // Set the new position of the button on dragging
        {
            self.setPositionOfButton(pan)
        }
    }
    
    // P1: Drag the button and display the degree as title of the button
    func setPositionOfButton(_ pan: UIPanGestureRecognizer) {
        
        // Set the center of the `dragMeButton`
        let location = pan.location(in: self.view)
        self.dragMeButton.center = location
        
        // Find the vector from dragMeButton'center to wheel's center
        let v1 = CGVector(dx: self.dragMeButton.center.x - self.wheel.center.x, dy:  self.dragMeButton.center.y - self.wheel.center.y)
        
        // Find the angle in radian
        let angle = atan2(v1.dy, v1.dx)
        
        // Convert the radian to degree
        var deg = angle * CGFloat(180.0 / Double.pi)
        
        // As the degree can be negative -> Convert it into positive by adding 360 into it.
        if deg < 0 { deg += 360.0 }
        
        // Set the integer value of the degree as the title of the button
        self.dragMeButton.setTitle("\(Int(deg))", for: .normal)
        
        // Update the `buttonDegree` variable
        self.buttonDegree = Int(deg)
    }
    
    // P2: Set the image to the background.
    func setBackgroundImage() {
        
        // Set the image counter on the basis of the drag of the button
        // With every drag the counter will increase its value and display the next image
        // It will reset itself when reaches at the end of the array
        self.setImageIndexCounter()
        
        // Fetch the image at the imageCounter index and display it in the `backgroundImageView`
        if self.imageArray.count > self.imageCounter {
            self.backgroundImageView.image = self.imageArray[self.imageCounter]
        }
    }
    
    // P1: Rotate the button to the random amount at every tap
    func rotateButtonRandomAmount() {
        
        // Generate random number within the available degree
        let number = (0 ... 360).randomElement()
        
        // Add it to the current degree the number is at
        self.buttonDegree = (buttonDegree ?? 0) + (number ?? 0)
        
        if (self.buttonDegree ?? 0) > 360 {
            self.buttonDegree = 0
        }
        
        // Rotate the button to the updated degree
        //self.dragMeButton.transform = self.dragMeButton.transform.rotated(by: CGFloat(Double(self.buttonDegree ?? Int(0.0)) / 180.0 * .pi))
    }
}

// MARK: This Extension contains the methods associated with the Photo Library
extension ViewController {
    
    // P2: Fetch random images from Photo Gallery
    func getImagesFromPhotos(numberOfImages: Int) -> [UIImage]? {
        
        let imgManager = PHImageManager.default()
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        
        // Fetch all the assets of type image
        let fetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        
        // Initalialize an array to store the images
        var allImages = [UIImage]()
        
        // Fetch Random Indexes from the fetched result assets. It should not be more than numberOfImages parameter.
        self.imageArrayIndex = (0 ... fetchResult.count - 1).randomElements(numberOfImages)
        
        // Fetch the image from the generated indexes.
        for index in imageArrayIndex ?? [Int]() {
            
            let asset = fetchResult.object(at: index) as PHAsset
            
            imgManager.requestImage(for: asset, targetSize: CGSize(width:200,height:200), contentMode: .aspectFill, options: requestOptions, resultHandler: { (uiimage, info) in
                if let image = uiimage {
                    allImages.append(image)
                }
            })
        }
        
        // Return the random image array to display it in the `imageCollectionView`
        return allImages
    }
}

// MARK: This Extension contains the methods associated with the UICollectionView
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.ramdomImageView.image = self.imageArray[indexPath.row]
        return cell
    }
}

// MARK: Helper Methods
extension ViewController {
    
    // Set the counter to display the background image
    func setImageIndexCounter()  {
        
        // If the counter exceeds the total value of image available, reset it to 0
        if self.imageCounter >= self.imageArray.count {
            self.imageCounter = 0
            return
        }
        self.imageCounter += 1
    }
}
